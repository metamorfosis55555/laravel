<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVentaproductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventaproductos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cliente')->nullable();
            $table->string('producto')->nullable();
            $table->string('discripcion')->nullable();
            $table->bigInteger('cantidad')->nullable();
            $table->bigInteger('precio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventaproductos');
    }
}
